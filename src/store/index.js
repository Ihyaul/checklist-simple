import { createStore } from "vuex";
// import Vue from "vue";
// import Vuex from "vuex";

// Vue.use(Vuex);

const axios = require("axios");

export default createStore({
  state: {
    token: localStorage.getItem("token") ?? null,
    authDataForm: {
      email: "",
      password: "",
    },
  },

  getters: {
    token: (state) => state.token,
  },

  mutations: {
    SET_AUTH_DATA_FORM: (state, target) => {
      state.authDataForm = {
        ...state.authDataForm,
        [target.name]: target.value,
      };
    },

    SET_AUTH: (state, { token }) => {
      state.token = token;

      localStorage.setItem("token", token);
    },
  },

  actions: {
    login: async ({ commit }, payload) => {
      try {
        const data = axios.post("http://18.139.50.74:8080/login", payload);
        commit("SET_AUTH_DATA_FORM", data);
        //console.log(this.state.authDataForm);
        commit("SET_AUTH", data);
        // console.log(data.data.data.token);
        console.log("=-=");
        console.log(data);
        //console.log(data);
        return Promise.resolve(data);
      } catch (err) {
        return Promise.reject(err);
      }
    },
    REFRESH_TOKEN: () => {
      return new Promise((resolve, reject) =>
        axios
          .post(`token/refresh`)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          })
      );
    },
  },
  modules: {},
});
