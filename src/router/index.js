import { createRouter, createWebHashHistory } from "vue-router";
import Login from "../views/Login.vue";
import DashboardChecklist from "../views/DashboardChecklist.vue";
import ChecklistDetail from "../views/ChecklistDetail.vue";

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/Dashboard",
    name: "DashboardChecklist",
    component: DashboardChecklist,
  },
  {
    path: "/ChecklistDetail",
    name: "ChecklistDetail",
    component: ChecklistDetail,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
